#include "ofApp.h"

void ofApp::setup() {

  dmx.connect("tty.usbserial-EN143965"); // use the name
  //dmx.connect(0); // or use a number

  /*
  for (int i=0; i < nbchannels; i++ ) {
    ofParameter<int> chan;
    //channels.push_back(new ofParameter<int>());
    channels.push_back(chan);
    panel.add(chan);
    chan.set("Channel "+ofToString(i+1), 120, 0, 255);
  }
  */
  
  chan1.set("Channel 1", 120, 0, 255);
  chan2.set("Channel 2", 120, 0, 255);
  chan3.set("Channel 3", 120, 0, 255);
  chan4.set("Channel 4", 120, 0, 255);
  chan5.set("Channel 5", 120, 0, 255);
  chan6.set("Channel 6", 120, 0, 255);
  chan7.set("Channel 7", 120, 0, 255);
  chan8.set("Channel 8", 120, 0, 255);
  chan9.set("Channel 9", 120, 0, 255);
  chan10.set("Channel 10", 120, 0, 255);
  chan11.set("Channel 11", 120, 0, 255);
  chan12.set("Channel 12", 120, 0, 255);

  autoCycle.set("AutoCycle", false);
  panel.setName("DMX control");
  panel.setup();

  panel.add(chan1);
  panel.add(chan2);
  panel.add(chan3);
  panel.add(chan4);
  panel.add(chan5);
  panel.add(chan6);
  panel.add(chan7);
  panel.add(chan8);
  panel.add(chan9);
  panel.add(chan10);
  panel.add(chan11);
  panel.add(chan12);
  
  panel.add(autoCycle);
}

void ofApp::update() {

	// use the time to generate a level
    if (autoCycle) {
        chan1 = 127 + 127 * sin(2 * ofGetElapsedTimef());
        chan2 = 127 + 127 * sin(-2 * ofGetElapsedTimef());
        chan3 = 127 + 127 * sin(1.5 * ofGetElapsedTimef());
    }
    /*
    dmx.setLevel(1, chan1);
    dmx.setLevel(2, chan2);
    dmx.setLevel(3, chan3);
    dmx.setLevel(4, chan4);
    dmx.setLevel(5, chan5);
    dmx.setLevel(6, chan6);
    dmx.setLevel(7, chan7);
    dmx.setLevel(8, chan8);
    dmx.setLevel(9, chan9);
    dmx.setLevel(10, chan10);
    dmx.setLevel(11, chan11);
    dmx.setLevel(12, chan12);
    */
    /*
    for (int i=0; i < nbchannels; i++ ) {
     }
    */
    dmx.update();
}

void ofApp::draw() {

	ofSetColor(chan1);
	ofDrawRectangle(0, 0, ofGetWidth() / 3, ofGetHeight());
	
	ofSetColor(chan2);
	ofDrawRectangle(ofGetWidth() / 3, 0, ofGetWidth() / 3, ofGetHeight());
    
    ofSetColor(chan3);
    ofDrawRectangle(2*(ofGetWidth() / 3), 0, ofGetWidth() / 3, ofGetHeight());
    
    panel.draw();
}
