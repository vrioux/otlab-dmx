(ql:register-local-projects)
(ql:quickload :sccl)
(in-package :sccl)


;; faire 3 niveaux


#|
diskutil erasevolume HFS+ 'tmpram' `hdiutil attach -nomount ram://2048`
cd /Volumes/tmpram
rm -rf .{,_.}{fseventsd,Spotlight-V*,Trashes}
mkdir .fseventsd
touch .fseventsd/no_log .metadata_never_index .Trashes
|#

(g

 (defparameter *mess* "")
 (defun send (message) (defparameter *mess* (+str *mess* message " " )))
 (defparameter *tmproot* "/Volumes/tmpram/")

 (fork :send
			 (loop
					(when (not (eq *mess* ()))
						(with-open-file (stream (+str *tmproot* "sun7cl-in") :direction :output :if-exists :supersede) (format stream *mess*))
						(defparameter *mess* "")
						;; remove .fseventsd
						(su:run-program (+str "rm -r " *tmproot* ".fseventsd/*"))
						)
					(sleep 0.01)
					)
			 )

 (defun edge (n i) (send (+str n " " i))) ; n edge number, i intensity
 (defun on (n &optional (i 255)) (edge n i))
 (defun gr (n &optional (i 10)) (edge n i)) ; grey
 (defun off (n) (edge n 0))
 (defun grey (i) (loop for e from 1 to 12 do (gr e i)))
 (defun white () (loop for e from 1 to 12 do (on e)))
 (defun black () (loop for e from 1 to 12 do (off e)))

 (defun rand (delay)
	 (fork :rand
				 (loop
						(let ((e (rri 1 12)))
							(edge e (rr 1 20))
							(sleep delay)
							(off e)
							))))
 
 
 (defun maintain-black (duration)
	 (fork :black
				 (loop for i from 0 to (* duration 1000) do
							(black)
							(sleep 0.0001)
							)))

 (defun carre (a b c d intensity)
	(gr a intensity)
	(gr b intensity)
	(gr c intensity)
	(gr d intensity))

 (defun edge-rand ()
	(fork :edge-rand
				(loop
					 (let ((e (rri 1 12)))
						 (edge e (rr 1 10))
						 (sleep (rr 0.1 30))
						 (off e)
						 (sleep (rr 1 10))
						 ))))

 )

;; test
(white)
(black)

(gr 1)
(gr 2)
(gr 3)
(gr 4)
(gr 5) ;
(gr 6) ;
(gr 7)
(gr 8) ;
(gr 9)
(gr 10)
(gr 11) ;
(gr 12) ;


(fork :cligno-all
			(loop
				 (loop for i from 1 to 12 do (on i))
				 (sleep 0.01)
				 (loop for i from 1 to 12 do (off i))
				 (sleep 0.01)
				 ))
(kfork :cligno-all)

;; 50 cubes un des 50 intensité au dessus

(fork :intro
			(loop
				 (let ((period (rr 0.01 10))
							 (dur-noir (rr 0.1 10))
							 )
					 (rand period)
					 (sleep (rr 10 30))
					 (maintain-black dur-noir)
					 (sleep dur-noir))))

(kfork :intro)
(kfork :black)
(kfork :rand)
(rand 0.1)
(kfork :intro)
(kfork :black)
(kfork :rand)
(kfork :edge-rand)
(black)

(edge-rand)
(rand 0.1)
(kfork :rand)

(rand 1)
(defun tranche (delay intensity)
	(fork :tranche (loop
										(grey intensity)
										(sleep delay)
										(black)

										)))

(kfork :rand)


(fork :r2
			(loop
				 (let ((e (rri 1 4)))
					 (on e)
					 (sleep (rr 0.1 0.3))
					 (off e)
					 )))
(kfork :r2)

(fork :plafond
			(loop
				 (let ((e (rri 1 4)))
					 (gr e (rr 1 6))
					 (sleep (rr 1 3))
					 (off e)
					 )))
(kfork :plafond)

(fork :helice-plafond
			(loop
				 (loop for e from 1 to 4 do
							(gr e 1)
							(gr (+ e 4) 100)
							(sleep 0.1)
							(off e)
							(off (+ e 4))
							)))
(kfork :helice-plafond)

(fork :pilier
			(loop
				 (loop for e from 9 to 12 do
							(gr e (rr 0 2))
						;;(sleep (rr 2 25))
							(sleep 0.01)
							(off e)
							)))
(kfork :pilier)
(black)
(fork :helice-bas
			(loop
				 (loop for e from 5 to 8 do
							(gr e (rr 1 6))
							(sleep 0.01)
							(off e)
							)))
(kfork :helice-bas)

(fork :plafond
			(loop
				 (loop for i from 1 to 255 do
							(loop for e from 1 to 4 do
									 (gr e i)
									 (sleep 0.01)
									 ))
				 (black)
				 (sleep 5)
				 ))
(kfork :plafond)

(defun bas (delay intensity)
	(fork :bas
				(loop
					 (loop for e from 5 to 8 do
								(gr e intensity)
								(sleep delay)
								(off e)
								))
				))

(defun cligno (edge delay-on delay-off intensity)
	(fork :cligno
				(loop
					 (gr edge intensity)
					 (sleep delay-on)
					 (off edge)
					 (sleep delay-off)
					 ))
	)

(defun carre1 (intensity)
	(gr 1 intensity)
	(gr 9 intensity)
	(gr 5 intensity)
	(gr 10 intensity)
	)


(defun 6bords (a b c d e f intensity)
	(carre a b c d intensity)
	(gr e intensity)
	(gr f intensity)
	)

(fork :6bords
			(loop
				 (6bords (rri 1 3) 4 5 (rri 6 8) (rri 9 11) 12 10)
				 (sleep (rr 0.5 2))
				 (black)
				 )
			)
(kfork :6bords)

(black)
(g
 (gr 1 1)
 (gr 7 1)
 )
(g
 (gr 9 1)
 (gr 11 1)
 )
(carre 1 9 5 10 1) ; face
(carre 5 6 7 8 1) ; bas
(carre 1 2 3 4 1) ; haut
(carre 10 8 11 2 1) ; gauche
(carre 12 4 9 6 1) ; droit



(fork :cl-carre
			(loop
				 (carre 5 6 7 8 1)
				 (sleep (rr 20 50))
				 (loop for i from 0 to 2 do
							(black)
							(carre 1 2 3 4 1) ; haut
							(sleep (rr 0.01 0.1))
							(black)
							(sleep (rr 0.01 0.03))
							)
				 (black)
				 ))


(black)
(white)
(rand 0.01)
(kfork :rand)

(bas 2 10)
(cligno 1 1 10 1)
(cligno 2 1 2 1)

(g
 (kfork :cl-carre)
 (kfork :edge-rand)
 (kfork :rand)
 (kfork :bas)
 (kfork :pilier)
 (kfork :plafond)
 (kfork :cligno)
 (kfork :black)
 (kfork :cl-carre)
 (kfork :rand)
)
