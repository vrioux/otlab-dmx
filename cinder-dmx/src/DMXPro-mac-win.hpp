/*
 *  DMXPro.hpp
 *
 *  Created by Andrea Cuius
 *  The MIT License (MIT)
 *  Copyright (c) 2014 Nocte Studio Ltd.
 *
 *  www.nocte.co.uk
 *
 * adapted vrx 2018
 *
 */

#pragma once

#include "cinder/Thread.h"
#include "cinder/Serial.h"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"

#define DMXPRO_START_MSG		        0x7E		// Start of message delimiter
#define DMXPRO_END_MSG			        0xE7		// End of message delimiter
#define DMXPRO_SEND_LABEL		        6			// Output Only Send DMX Packet Request
#define DMXPRO_RECEIVE_ON_CHANGE_LABEL  8
#define DMXPRO_RECEIVE_PACKET_LABEL     5
#define DMXPRO_BAUD_RATE		        57600		// virtual COM doesn't control the usb, this is just a dummy value
#define DMXPRO_FRAME_RATE		        35			// dmx send frame rate
#define DMXPRO_DATA_SIZE       	        513         // include first byte 0x00, what's that?
#define DMXPRO_PACKET_SIZE              518         // data + 4 bytes(DMXPRO_START_MSG, DMXPRO_SEND_LABEL, DATA_SIZE_LSB, DATA_SIZE_MSB) at the beginning and 1 byte(DMXPRO_END_MSG) at the end

//////////////////////////////////////////////////////////
// LAST 4 dmx channels seem not to be working, 508-511 !!!
//////////////////////////////////////////////////////////

class DMXPro;
typedef std::shared_ptr<DMXPro> DMXProRef;

class DMXPro{

public:

	enum DeviceMode {
		SENDER,
		RECEIVER
	};

	unsigned char   mDMXDataIn[512];                        // Incoming data parsed by the thread
	unsigned char	mDMXPacketIn[DMXPRO_PACKET_SIZE];		// Incoming DMX packet, it contains bytes
	unsigned char	mDMXPacketOut[DMXPRO_PACKET_SIZE];		// Outgoing DMX packet, it contains bytes
	ci::SerialRef	mSerial;                                // serial interface
	int				mSenderThreadSleepFor;                  // sleep for N ms, this is based on the FRAME_RATE
	std::mutex      mDMXDataMutex;                          // mutex unique lock
	std::string		mSerialDeviceName;                      // usb serial device name
	std::thread     mDataThread;
	bool            mRunDataThread;
	DeviceMode      mDeviceMode;
    
	static DMXProRef create( const std::string &deviceName, DeviceMode mode = DeviceMode::SENDER ) {
		return DMXProRef( new DMXPro( deviceName, mode ) );
	}

	DMXPro( const string &deviceName, DeviceMode mode ) : mSerialDeviceName(deviceName) {
    mDeviceMode             = mode;
    mSerial                 = nullptr;
		mSenderThreadSleepFor   = 1000 / DMXPRO_FRAME_RATE;
    
		init();
		
		setZeros();
	}
	
	~DMXPro() {
		shutdown(true);
	};

	static void listDevices() {
		const std::vector<ci::Serial::Device> &devices( ci::Serial::getDevices(true) );
		ci::app::console() << "--- DMX usb pro > List serial devices ---" << std::endl;
		for( std::vector<ci::Serial::Device>::const_iterator deviceIt = devices.begin(); deviceIt != devices.end(); ++deviceIt )
			ci::app::console() << deviceIt->getName() << std::endl;
		ci::app::console() << "-----------------------------------------" << std::endl;
	}

	static std::vector<std::string> getDevicesList() {
		std::vector<std::string> devicesList;
		const std::vector<ci::Serial::Device> &devices( ci::Serial::getDevices(true) );
		for( std::vector<ci::Serial::Device>::const_iterator deviceIt = devices.begin(); deviceIt != devices.end(); ++deviceIt )
			devicesList.push_back( deviceIt->getName() );
		return devicesList;
	}

	void	shutdown(bool send_zeros = true) {
		console() << "DMXPro shutting down.." << endl;
		if ( mSerial ) {
			if (send_zeros) setZeros();					// send zeros to all channels
			ci::sleep( mSenderThreadSleepFor * 2 );
			mRunDataThread = false;
			if ( mDataThread.joinable() ) {
				console() << "thread is joinable" << endl;
				mDataThread.join();
			}
			else console() << "cannot join thread!" << endl;
			mSerial->flush();
			mSerial = nullptr;
		} else {
			mDataThread.detach();
		}
    console() << "DMXPro > shutdown!" << endl;
	}

	void init(bool initWithZeros = true){
		console() << "DMXPro > Initializing device" << endl;
		initDMX();
    initSerial(initWithZeros);
    mDataThread = std::thread( &DMXPro::processDMXData, this );

	};

	void initSerial(bool initWithZeros){
		if ( mSerial ) {
			if (initWithZeros) {
				setZeros();					// send zeros to all channels
				console() << "DMXPro > Init serial with zeros() before disconnect" << endl;
				ci::sleep(100);	
			}
			mSerial->flush();
			mSerial = nullptr;
			ci::sleep(50);	
		}
	
		try {
			Serial::Device dev = Serial::findDeviceByNameContains(mSerialDeviceName);
    
			if ( dev.getName() == "" ) {
				console() << "DMXPro > cannot open device, device not found! > " << mSerialDeviceName << endl;
				return;
			}

			mSerial = Serial::create( dev, DMXPRO_BAUD_RATE );
			console() << "DMXPro > Connected to usb DMX interface: " << dev.getName() << endl;
		}
		catch( ... ) {
			console() << "DMXPro > There was an error initializing the usb DMX device" << endl;
			mSerial = nullptr;
		}
	};

	void initDMX(){
		// LAST 4 dmx channels seem not to be working, 508-511 !!!

		// initialize all channels with zeros, data starts from [5]
    for (int i=0; i < DMXPRO_PACKET_SIZE; i++) mDMXPacketOut[i] = 0;

		// fill it
    mDMXPacketOut[0] = DMXPRO_START_MSG;							// DMX start delimiter 0x7E
		mDMXPacketOut[1] = DMXPRO_SEND_LABEL;							// set message type
    mDMXPacketOut[2] = (int)DMXPRO_DATA_SIZE & 0xFF;				// Data Length LSB
    mDMXPacketOut[3] = ((int)DMXPRO_DATA_SIZE >> 8) & 0xFF;         // Data Length MSBc
		mDMXPacketOut[4] = 0;                                           // DMX start code
		mDMXPacketOut[DMXPRO_PACKET_SIZE-1] = DMXPRO_END_MSG;           // DMX start delimiter 0xE7
    
    // init incoming DMX 512 data 
    for( size_t k=0; k < 512; k++ ) mDMXDataIn[k] = 0;
	};

	void	setZeros() {
		// DMX channels start form byte [5] and end at byte [DMXPRO_PACKET_SIZE-2], last byte is EOT(0xE7)
		for (int i=5; i < DMXPRO_PACKET_SIZE-2; i++)                        
			mDMXPacketOut[i] = 0;
	};


	void processDMXData() {
		console() << "DMXPro::processDMXData() start thread" << endl;
		mRunDataThread = true;
		if ( mDeviceMode == DeviceMode::SENDER ) {
			while( mSerial && mRunDataThread ) {
				std::unique_lock<std::mutex> dataLock(mDMXDataMutex); // get DMX packet UNIQUE lock
				mSerial->writeBytes( mDMXPacketOut, DMXPRO_PACKET_SIZE ); // send data
				dataLock.unlock(); // unlock data
				std::this_thread::sleep_for( std::chrono::milliseconds( mSenderThreadSleepFor ) );
			}
    }
		else if ( mDeviceMode == DeviceMode::RECEIVER ) {
			unsigned char   value = '*';    // set to something different than packet label or start msg
			uint32_t        packetDataSize;
			while( mSerial && mRunDataThread ) {
				// wait for start message and label		    
				/*
					while ( value != DMXPRO_START_MSG && mSerial->getNumBytesAvailable() > 0 )
					{
					value = mSerial->readByte();
					}
                
					if (  mSerial->getNumBytesAvailable() == 0 || mSerial->readByte() != DMXPRO_RECEIVE_PACKET_LABEL )
					{
					value = '*';
					continue;
					}
				*/
				while ( mRunDataThread && value != DMXPRO_RECEIVE_PACKET_LABEL ) {
					while ( mRunDataThread && value != DMXPRO_START_MSG ) {
						if ( mSerial->getNumBytesAvailable() > 0  )
							value = mSerial->readByte();
					}
					if ( mSerial->getNumBytesAvailable() > 0  )
						value = mSerial->readByte();
				}            
				// read header
				if ( mSerial->getNumBytesAvailable() < 2 )
					continue;
            
				packetDataSize = mSerial->readByte();                       // LSB
				packetDataSize += ( (uint32_t)mSerial->readByte() ) << 8;   // MSB

				// Check Length is not greater than allowed
				if ( packetDataSize <= 514 ) // dmx data + 2 start zeros
					{
						// Read the actual Response Data
						mSerial->readAvailableBytes( mDMXPacketIn, packetDataSize );
						// finally check the end code
						if ( mSerial->getNumBytesAvailable() > 0 && mSerial->readByte() == DMXPRO_END_MSG ) {
							// valid packet, parse DMX data
							// the first 2 bytes are 0(by specs there should only be 1 zero, not sure where the other one comes from!) 
							std::unique_lock<std::mutex> dataLock(mDMXDataMutex);
							for( size_t k=2; k < packetDataSize; k++ ) mDMXDataIn[k-2] = mDMXPacketIn[k];
							dataLock.unlock();
						} else value = '*'; // invalid packet, reset
					}
				else value = '*'; // invalid packet, reset
				std::this_thread::sleep_for( std::chrono::milliseconds( 16 ) );
			}
    }
    
		mRunDataThread = false;
    
		console() << "DMXPro > sendDMXData() thread exited!" << endl;
	}

	void	setValue(int value, int channel) {    
		if ( channel < 0 || channel > DMXPRO_PACKET_SIZE-2 ) {
			console() << "DMXPro > invalid DMX channel: " << channel << endl;
			return;
		}
		// DMX channels start form byte [5] and end at byte [DMXPRO_PACKET_SIZE-2], last byte is EOT(0xE7) 
		value = math<int>::clamp(value, 0, 255);
		std::unique_lock<std::mutex> dataLock(mDMXDataMutex);			// get DMX packet UNIQUE lock
		mDMXPacketOut[ 5 + channel ] = value;                                  // update value
		dataLock.unlock();													// unlock mutex
	};
	
	bool	isConnected() { return mSerial != NULL; }
	
	
	size_t  getValue( int channel ) {    
		if ( channel < 0 || channel > 512 ) {
			console() << "DMXPro > invalid DMX channel: " << channel << endl;
			return 0;
		}
	
		size_t val;
	
		std::unique_lock<std::mutex> dataLock(mDMXDataMutex);
		val = mDMXDataIn[channel];
		dataLock.unlock();	
	
		return val;
	}

	std::string  getDeviceName() { return mSerialDeviceName; }
	

    
    
};




