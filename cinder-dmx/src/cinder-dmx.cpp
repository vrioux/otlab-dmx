#include "vs-osc.hpp"
#include "enttecdmxusb.h"

/*
	entre 0.2 et 0.4 ms to send 10 values
	ccl : entre 1ms et 2ms à envoyer
	sbcl : entre 0.1ms et 0.8ms à envoyer
*/

class CinderDmxApp : public App {
public:	
	EnttecDMXUSB *interfaceDMX;
	VsOsc osc2;
	
	CinderDmxApp() : App() {}

	~CinderDmxApp() {
		osc2.close();
	}

	void setup() {
		//osc2.setup_sender("127.0.0.1", 3015); // 
		osc2.setup_receiver(9016);

		// interfaceDMX = new EnttecDMXUSB (DMX_USB_PRO, "/dev/ttyUSB0");
		interfaceDMX = new EnttecDMXUSB (DMX_USB_PRO, "/dev/dmx");
		// remet à zéro les valeurs des 512 canaux
    interfaceDMX->ResetCanauxDMX();
    // émet les valeurs des 512 canaux
    interfaceDMX->SendDMX();

		osc2.receiver
			->setListener( "/dmx",
										 [&]( const osc::Message &msg ){
											 clic();
											 int32_t nb_elt = msg[0].int32();
											 cout << "nombre d'éléments " << nb_elt << endl;
											 byte datas[nb_elt];
											 for (int i=0; i < nb_elt; i++) {
												 datas[i] = byte(msg[i+1].int32());
												 //cout << int(datas[i]) << endl;
											 }
											 
											 interfaceDMX->SendDatasDMX(datas,1,nb_elt);
											 clac();
										 });

		
		//DMXPro::listDevices();
		//mdmx  = DMXPro::create( "video0" );					
	}

	void draw() {
		gl::clear( Color(1,0,0) );
	}

};

CINDER_APP(CinderDmxApp, RendererGl())
