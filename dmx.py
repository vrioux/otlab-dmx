BAUDRATE=230400    #Baudrate for serial port in bits per second, typical: 115200, 230400, 460800
DMXbreak=9    #DMX output break time in 10.67 microsend units
DMXmark=1    #DMX output Mark After Break time in 10.67 microsend units
DMXrate=40    #DMX output rate in packets per second

import sys, serial, time
import sys, os
import liblo

def send(serial, flag, data):
    serial.write(chr(0x7E))
    serial.write(chr(flag))
    serial.write(chr(len(data) & 0xFF))
    serial.write(chr((len(data) >> 8) & 0xFF))
    for j in range(len(data)):
        serial.write(chr(data[j]))
    serial.write(chr(0xE7))
	
def sendStartValues():
  global SERIAL,DMXbreak,DMXmark,DMXrate
  tosend=[0,0,DMXbreak,DMXmark,DMXrate]
  send(SERIAL,4,tosend)

def channels(path,args,*msg):
    if CONNECTED:
        global DMXDATA, SERIAL
        vec = args[0]
        DMXDATA = [0]+vec # pourquoi un 0 devant ?
        
        # autres = vec[0:20]
        # vertes = vec[20:29]
        # # on decale les vertes car pb avec un dimmer dmx
        # DMXDATA = [0]+autres+[255,255]+vertes

        send(SERIAL,6,DMXDATA)
    else:
        print (path, args)

def main():
    global CONNECTED, STARTBYTE, CHANNELS, DMXDATA, SERIAL, BAUDRATE, CHANGED

    try:
        SERIAL = serial.Serial("/dev/ttyUSB0", BAUDRATE, timeout=1)
        sendStartValues()
        CONNECTED = True
    except:
        CONNECTED = False
    STARTBYTE=0
    CHANNELS=18
    CHANGED=False
    DMXDATA= [STARTBYTE]
    
    # OSC
    server = liblo.Server(9001)
    server.add_method("/channels",'b',channels)
    # send(SERIAL,6,[0]*512)
    while True:
        server.recv(100)
    SERIAL.close()

if __name__ == "__main__":
  main()

  
    
