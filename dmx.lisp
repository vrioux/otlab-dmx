(ql:register-local-projects)
(ql:quickload :vs)
(defparameter s (make-instance 'vs::Osc-sender :port 9016))

#|
(vs::send s "/dmx" (sc-osc::sc-encode-blob '(12 13)))
(vs::send s "/dmx" '(12 13 234 234 0))
(time (vs::send s "/dmx" 20 13 23 245 12 789 890 90 38 89 90 78 35 67 12 13 23 245 12 789 890 90 38 89 90 78 35 67 12))
|#

(defmacro d (nb-canaux &rest args)
	`(vs::send s "/dmx" ,nb-canaux ,@args)
	)

(defun w (dur1 &optional dur2)
	(if dur2
			(sleep (vs::rr dur1 dur2))
			(sleep dur1)
	))

(defun b (dur1 &optional dur2) (d 8 0 0 0 0 0 0 0 0) (w dur1 dur2))

(defun g (&rest body ) (progn body))

(g
 (b 3) ; noir de 3 secondes
 (d 1 500)
 (w 1)
 (d 6 0 0 0 0 0 500)
 )


(vs::kfork "loop1")

(vs::lf "loop1" 0
					(g
					 (b 0.1 0.3)
					 (d 4 500 500 500 500)
					 (w 1 3)
					 )
					)

(vs::kfork "loop2")
(vs::lf "loop2" 0
					(g
					 (b 0.1 0.3)
					 (d 4 500 500 500 500)
					 (w 1 3)
					 )
					)

(vs::kfork "loop3")

(vs::lf "loop3" 0
					(g
					 (b 0.01 0.1)
					 (d 1 (vs::rri 0 500))
					 (w 0.01 0.1)
					 )
					)
(d 8 0 0 0 0 0 0 0 0)
kill loop
